# coding: utf-8
from matplotlib import pyplot as plt
import numpy as np
import numpy.linalg as la
from scipy import signal
from scipy.interpolate import BSpline
from scipy.interpolate import make_interp_spline, BSpline
from scipy.interpolate import UnivariateSpline

def get_g(p,h,i0):
    ret = np.zeros((1000,))
    g = signal.gaussian(1000,std=p)
    for i in range(1000):
        if i+500-i0<1000:
            ret[i] = g[i+500-i0]
    return [h*e for e in ret]

def get_sampled_sig(p):
    ret = np.zeros((1000,))
    pos = [(197,20.144),(660,30.101)]
    for i,h in pos:
        ret += get_g(p,h,i)
    return ret

img = plt.imread("./test_target_density.png")
rows,cols,colors = img.shape
img_size = rows*cols*colors
img_1D_vector = img.reshape(img_size)
sig = []
for i in range(img.shape[1]):
    seq = np.where(img[:,i,3] != 0)[0]
    if len(seq):
        sig.append(min(seq))
msig = max(sig)
sig = [msig-s for s in sig]
sig = np.array(sig)
sig = sig[7:]
sig = sig[:-4]
sig = sig[:-2]
x = np.array(range(len(sig)))
spl = UnivariateSpline(x, sig)
xs = np.linspace(0,66,1000)
spl.set_smoothing_factor(4.0)
nmap = spl(xs)
nmapb = np.zeros(2000)
nmapb[500:1500] = nmap

def get_g(p,h,i0):
    ret = np.zeros((1000,))
    g = signal.gaussian(1000,std=p)
    for i in range(1000):
        if i+500-i0<1000:
            ret[i] = g[i+500-i0]
    return [h*e for e in ret]

def get_scatter(sigs,p_list):
    x,y = [],[]
    for s,p in zip(sigs,p_list):
        x.append(p)
        y.append(entropy(disc_normalize(cam(nmapb,s))))
    return x,y

def get_sampled_sig(p):
    ret = np.zeros((1000,))
    pos = [(197,20.144),(660,30.101)]
    for i,h in pos:
        ret += get_g(p,h,i)
    return ret

def plot_cams(sigs,p_list):
    cams = [disc_normalize(cam(nmapb,s)) for s in sigs]
    for c in zip(cams,p_list):
        plt.plot(c,label="$\sigma=%s$"%(str(p)))
    plt.xlabel("Samples")
    plt.ylabel("Probabilities")
    plt.grid(True)
    plt.title("cam score for different parameters")

def plot_parameters(sigs,p_list):
    plt.plot(nmap,linewidth=4)
    for i,(sig,p) in enumerate(zip(sigs,p_list)):
        plt.plot(sig,label="$\sigma = %s$"%str(p))
        plt.fill_between(np.linspace(0,1000,1000),sig,color="C"+str(i+1),alpha=0.1)
    plt.xlim(0,1000)
    plt.ylim(0)
    plt.legend(loc="upper left")
    plt.title("Densities corresponding to different parameters")

def scatter_plot(xs,ys,p_list):
    for i,(x,y,p) in enumerate(zip(xs,ys,p_list)):
        plt.scatter([x],[y],color="C"+str(i+1),label="$\sigma=%s$"%str(p))
    plt.xlabel("Parameter value")
    plt.ylabel("Entropy")
    plt.grid(True)
    plt.legend(loc="upper left")
    plt.title("Entropy for different parameter values")

def plot_cams(sigs,p_list):
    cams = [disc_normalize(cam(nmapb,s)) for s in sigs]
    for i,(c,p) in enumerate(zip(cams,p_list)):
        plt.plot(c,color="C"+str(i+1),label="$\sigma=%s$"%(str(p)))
    plt.xlabel("Samples")
    plt.ylabel("Probability")
    plt.xlim(0,1000)
    plt.ylim(0,0.003)
    plt.grid(True)
    plt.title("cam score distribution for different parameters")
    plt.legend(loc="upper left")

def arrow_img(sigs,ax=None):
    plt.plot(nmapb,label="target density map")
    plt.fill_between(np.linspace(0,len(nmapb),len(nmapb)),nmapb,color="C0",alpha=0.5)
    plt.plot(sigs[3],label="query denisty map")
    plt.fill_between(np.linspace(0,len(sigs[3]),len(sigs[3])),sigs[3],color="C1",alpha=0.5)
    if ax is None:
        ax = plt.axes()
    ax.arrow(250,22,200,0,head_width=1.2,head_length=30,ec='k',fc='k')
    plt.xlim(0)
    plt.ylim(0)
    plt.xlabel("Samples")
    plt.legend(loc="upper right")
    plt.title("cam score calculation of query signal")

def parameter_example():
    p_list = [20,50,70,100,130]
    sigs = [get_sampled_sig(p) for p in p_list]
    xs,ys = get_scatter(sigs,p_list)
    grid = plt.GridSpec(2,2,wspace=0.1,hspace=0.2)
    ax = plt.subplot(grid[0,0])
    arrow_img(sigs,ax)
    plt.subplot(grid[0,1])
    plot_parameters(sigs,p_list)
    plt.subplot(grid[1,0])
    plot_cams(sigs,p_list)
    plt.subplot(grid[1,1])
    scatter_plot(xs,ys,p_list)
    plt.show()

def ov(m,t):
    s = []
    u = [0]*(len(m))
    u[:] = m
    for i in range(len(m)-len(t)):
        v = [0]*len(u)
        v[i:i+len(t)] = t
        _s = np.dot(u,v)
        s.append(_s)
    return s

def entropy(s):
    _s = 0
    for e in s:
        if e != 0:
            _s += -e*np.log(e)
    return _s/np.log(len(s))

def cc(m,t):
    s = []
    u = [0]*(len(m))
    u[:] = m
    for i in range(len(m)-len(t)):
        v = [0]*len(u)
        v[i:i+len(t)] = t
        _s = np.dot(u,v)/(np.dot(u,u)*np.dot(v,v))
        s.append(_s)
    return s
         
def cam(m,t):
    s = []
    u = [0]*(len(m))
    u[:] = m
    for i in range(len(m)-len(t)):
        v = [0]*len(u)
        v[i:i+len(t)] = t
        cam_u = cam_h(u)
        cam_v = cam_h(v)
        _s = np.dot(cam_u,cam_v)/(la.norm(cam_u)*la.norm(cam_v))
        s.append(_s)
    return s

def disc_normalize(s):
    _min = min(s)
    _max = max(s)
    s = [e/(_max - _min) - _min/(_max-_min) for e in s]
    _s = sum(s,0)
    return [e/_s for e in s]

def example_entropy():
	np.random.seed(19680801)
	mu, sigma = 0, 10
	x1 = mu + sigma * np.random.randn(10000)
	x2 = np.random.uniform(-50,50,10000)
	# the histogram of the data
	fig, (ax1, ax2) = plt.subplots(1, 2)
	ax1.hist(x1, 50, density=True, facecolor='steelblue', alpha=0.75)
	ax2.hist(x2, 50, density=True, facecolor='steelblue', alpha=0.75)
	ax1.set_xlabel("Samples")
	ax2.set_xlabel("Samples")
	ax1.set_ylabel("Probability")
	ax2.set_ylabel("Probability")
	ax1.set_title("Example Distribution A")
	ax2.set_title("Example Distribution B")
	ax1.text(-40, .035, r'$H(X) = 0.60772$',fontsize=14)
	ax2.text(-10, .015, r'$H(X) = 0.96442$',fontsize=14)
	ax1.set_xlim(-50, 50)
	ax2.set_xlim(-50, 50)
	ax1.set_ylim(0,0.05)
	ax2.set_ylim(0,0.05)
	ax1.grid(True)
	ax2.grid(True)
	plt.show()

def triangle(base,height,res):
    _x = np.linspace(0,base,res)
    y = []
    f1 = lambda x: 2*height/base*x
    f2 = lambda x: -2*height/base*x +2*height
    for x in _x[:len(_x)//2]:
        y.append(f1(x))
    for x in _x[len(_x)//2:]:
        y.append(f2(x))
    return _x,y       
    
def circle(base,res):
    _x = np.linspace(0,base,res)
    f = lambda x: np.sqrt((base/2)**2 - abs(base/2 - x)**2)
    return _x,[f(x) for x in _x]   
    
def cc(m,t):
    s = []
    u = [0]*(len(m))
    u[:] = m
    for i in range(len(m)-len(t)):
        v = [0]*len(u)
        v[i:i+len(t)] = t
        _s = np.dot(u,v)/(la.norm(u)*la.norm(v))
        s.append(_s)
    return s

cam_h = lambda v: v-np.mean(v)*np.ones_like(v)
         
def cam(m,t):
    s = []
    u = [0]*(len(m))
    u[:] = m
    for i in range(len(m)-len(t)):
        v = [0]*len(u)
        v[i:i+len(t)] = t
        cam_u = cam_h(u)
        cam_v = cam_h(v)
        _s = np.dot(cam_u,cam_v)/(la.norm(cam_u)*la.norm(cam_v))
        s.append(_s if _s>0 else 0)
    return s

def cam_mask(m,t):
    eps = 10e-12
    s = []
    u = np.array([0.0]*(len(m)))
    for i in range(len(m)-len(t)):
        u[:] = m
        v = np.array([0.0]*len(u))
        v[i:i+len(t)] = t
        u[np.where(v<=eps)] = 0.0
        cam_u = cam_h(u)
        cam_v = cam_h(v)
        if not (la.norm(cam_u) == 0 or la.norm(cam_v) == 0):
            _s = np.dot(cam_u,cam_v)/(la.norm(cam_u)*la.norm(cam_v))
            s.append(_s if _s>0 else 0)
        else:
            s.append(0)
    return s

def cam_mask(m,t):
    eps = 10e-12
    s = []
    u = np.array([0.0]*(len(m)))
    for i in range(len(m)-len(t)):
        u[:] = m
        v = np.array([0.0]*len(u))
        v[i:i+len(t)] = t
        u[np.where(v<=eps)] = 0.0
        cam_u = cam_h(u)
        cam_v = cam_h(v)
        if not (la.norm(cam_u) == 0 or la.norm(cam_v) == 0):
            _s = np.dot(cam_u,cam_v)/(la.norm(cam_u)*la.norm(cam_v))
            s.append(_s if _s>0 else 0)
        else:
            s.append(0)
    return s

def kbl(s1,s2):
    _s = 0
    for e1,e2 in zip(s1,s2):
        if (e1 != 0 and e2 !=0):
            _s += -e1*np.log(e1/e2)
    return _s

def example_score_difference():
	tri = triangle(2,2,200)
	cir = circle(4,400)
	dent = triangle(1.5,1.5,200)[1]
	y = [0.0]*1000
	y[200:400] = tri[1]
	y[600:1000] = cir[1]
	for i in range(len(dent)):
	    y[700+i] -= dent[i]
	dmap = y
	quer = dmap[600:]
	
	scc = cc(dmap,quer)
	scam = cam(dmap,quer)
	scamm = cam_mask(dmap,quer)
	nscc = disc_normalize(scc)
	nscam = disc_normalize(scam)
	nscamm = disc_normalize(scamm)
	enscc = entropy(nscc)
	enscam = entropy(nscam)
	enscamm = entropy(nscamm)
	kbl_scc_scam = kbl(nscc,nscam)
	kbl_scc_scamm = kbl(nscc,nscamm)
	print("enscc,enscam,enscamm",enscc,enscam,enscamm)	
	print("kbl_scc_scam,kbl_scc_scamm",kbl_scc_scam,kbl_scc_scamm)	

	plt.subplots_adjust(bottom=0.05,top=0.95,right=0.95,left=0.075,hspace=0.225,wspace=0.1)	
	ax = plt.subplot(121)
	plt.plot(nscc,label="cc-score")
	plt.plot(nscam,label="cam-score")
	plt.plot(nscamm,label="cam-maksed-score")
	plt.title("score distributions")
	plt.legend(loc="upper left")
	plt.xlabel("Samples")
	plt.ylabel("Probability")
	ax = plt.subplot(222)
	ax.set_xlim(-10,1010)
	ax.set_ylim(0,2.1)
	plt.plot(dmap,color="steelblue")
	plt.fill_between(np.linspace(0,len(dmap),1000),dmap,color="steelblue",alpha=0.5)
	plt.title("target density map",fontsize=10)
	ax = plt.subplot(224)
	plt.plot(quer,color="red")
	plt.fill_between(np.linspace(0,len(quer),400),quer,color="red",alpha=0.35)
	ax.set_xlim(-10,1010)
	ax.set_ylim(0,2.1)
	plt.plot(np.linspace(0,len(quer),len(quer)),quer,color="red",alpha=0.35)
	plt.title("query density map",fontsize=10)

def half_triangle(base,height,res):
    _x = np.linspace(0,base,res)
    y = []
    f1 = lambda x: height/base*x
    for x in _x:
        y.append(f1(x))
    y.reverse()
    return _x,y

def make_signal():
    cir = circle(4,400)
    tri = triangle(2,2,200)
    y = [0.0]*1000
    y[600:1000] = cir[1]
    y[200:400] = tri[1]
    return y

def make_conf_example():
    quer = circle(4,400)[1]
    quer2 = circle(4,400)[1]
    dent = triangle(1.5,1.5,200)[1]
    dent2 = half_triangle(1.5,1.5,100)[1]
    dent2.reverse()
    for i in range(len(dent2)):
        quer2[100+i] -= dent2[i]
    for i in range(len(dent)):
        quer[100+i] -= dent[i]
    dmap = make_signal()
    for i in range(len(dent)):
        dmap[700+i] -= dent[i]
    grid = plt.GridSpec(2,4,wspace=0.2,hspace=0.3)
    plt.subplot(grid[:2,:2])
    plt.plot(disc_normalize(cam(dmap,quer)),label="confirmation 1")
    plt.plot(disc_normalize(cam(dmap,quer2)),label="confirmation 2")
    print("Entropy query1: %s"%str(entropy(disc_normalize(cam(dmap,quer)))))
    print("Entropy query2: %s"%str(entropy(disc_normalize(cam(dmap,quer2)))))
    plt.title("cam-score for different confirmations")
    plt.xlabel("Samples")
    plt.ylabel("Probability")
    plt.legend(loc="upper left")
    plt.grid(True)
    plt.subplot(grid[:1,2:4])
    plt.plot(dmap,color="steelblue")
    plt.fill_between(np.linspace(0,len(dmap),1000),dmap,color="steelblue",alpha=0.5)
    plt.title("target map density")
    ax = plt.subplot(grid[1,2])
    plt.plot(quer)
    plt.fill_between(np.linspace(0,len(quer),len(quer)),quer,alpha=0.5)
    plt.title("query map conformation 1",fontsize=10)
    ax.set_ylim(0,2.1)
    ax = plt.subplot(grid[1,3])
    plt.plot(quer2,color="C1")
    plt.fill_between(np.linspace(0,len(quer2),len(quer)),quer2,color="#ff7f0e",alpha=0.5)
    plt.title("query map conformation 2",fontsize=10)
    ax.set_ylim(0,2.1)
    plt.show()

def a_k(f,k,N):
    ret = f(1)/2 + f(-1)/2*(-1)**k + sum([f(np.cos(n*np.pi/N))*np.cos(n*k*np.pi/N) for n in range(1,N)])
    ret *= 2/N
    return ret

def eval_points(f,N):
    xs = [-1] + [np.cos(n*np.pi/N) for n in range(1,N)] + [1]
    ys = [f(_x) for _x in xs]
    return xs,ys

def cheby_approx(nmapn,N):
    f_ex = lambda x: np.interp([x],xs,nmapn)[0]
    ts  = [sympy.polys.orthopolys.chebyshevt_poly(n,x) for n in range(N)]
    f_t = sum([a_k(f_ex,0,N)/2*ts[0]]+[a_k(f_ex,i,N)*ts[i] for i in range(1,N)])
    fig,ax = plt.subplots()
    plt.plot(xs,nmapn)
    plt.plot(xs,[f_t.subs(x,_x) for _x in xs])
    pxs,pys = eval_points(f_ex,N)
    for _x,_y in zip(pxs,pys):
        ax.add_artist(plt.Circle((_x,_y),0.02,color='r'))
    fig.savefig("cheby-%s.png"%str(N))

if __name__ == "__main__":
	#example_entropy()
	#example_score_difference()
	#make_conf_example()
	#parameter_example()
	pass
